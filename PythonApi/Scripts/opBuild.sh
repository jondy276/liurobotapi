#!/bin/sh

if [ $# -ne 0 ];
then 
    echo "Builds the api for use with darwin"
    echo "usage: ./opBuild.sh"
    exit 1
fi

# Get path to api
API_HOME=$(dirname $(dirname $(readlink -f $0)))

# Make sure it's clear what platform the api is built for
echo "darwin" > "$API_HOME"/current_platform

# Build lunatic
sh "$API_HOME/Scripts/lunaticBuild.sh"

# Rsync PythonApi to darwin 
# (exluding UPennalizer)
rsync -a --progress --exclude 'UPennalizers' "$API_HOME" student@darwin:~/

# Move into UPenn lib 
cd "$API_HOME"/UPennalizers/Lib/

if [ ! -f "$API_HOME"/current_platform ] ||
    [ ! $(cat "$API_HOME"/current_platform) = "darwin" ];
then
    # Cleanup in UPenn
    # (it might contain files for a different platform)
    make clean
fi

# Build the UPennalizer code
make setup_op

# Rsync UPennalizer code to robot 
# (the newly built player folder)
rsync -a --progress "$API_HOME"/UPennalizers/Player student@darwin:~/PythonApi/UPennalizers

exit 0