#!/bin/sh

# Runs a python program locally together with the api
# But does not start dcm or webots

if [ $# -ne 1 ];
then
    echo "Runs a program locally with the api"
    echo "Usage: ./localRun.sh /path/to/code/folder/"
    exit 1
fi

# Create some usefull variables
API_HOME=$(dirname $(dirname $(readlink -f $0)))
USER_PROGRAM_FOLDER=$(readlink -f $1)

# Check that the api is built for darwin inside webots 
if [ ! -f "$API_HOME"/current_platform ] ||
    [ ! $(cat "$API_HOME"/current_platform) = "webots_darwin" ];
then
    echo "The api is not built for use with webots" 
    exit 2
fi

# Set the env variable that controls which platform the api works against
# (for one, this controls the servo configuration which differs between webots and darwin)
export API_PLATFORM=$(cat "$API_HOME/current_platform")

# Add the lunatic API lib to the python path
export PYTHONPATH=$PYTHONPATH:$HOME/liurobotapi/PythonApi/Lib 
export PYTHONPATH=$PYTHONPATH:$HOME/liurobotapi/PythonApi 

# This is required to make the controller loadable from lua 
export LUA_CPATH=$HOME"/liurobotapi/PythonApi/Lib/?.so;"$LUA_CPATH

python -U "$USER_PROGRAM_FOLDER/main.py"

exit $?


