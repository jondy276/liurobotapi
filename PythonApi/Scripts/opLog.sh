#!/bin/sh

# A script to start logging on darwin
# (/used to create the logging data for image calibration)

API_HOME=$(dirname $(dirname $(readlink -f $0)))

# Add matlab to the path
# (must be mounted and sym-linked from the usb stick)
export PATH="/media/matlab/bin:$PATH"

# Check that the api is built for darwin
if [ ! -f "$API_HOME"/current_platform ] ||
    [ ! $(cat "$API_HOME/current_platform") = "darwin" ];
then
    echo "The api is not built for use on darwin" 
    exit 2
fi
# Set the env variable that controls which platform the api works against
# (for one, this controls the servo configuration which differs between webots and darwin)
export API_PLATFORM=$(cat "$API_HOME/current_platform")

# Build matlab stuff
# (abort on failure)
UPENN_MATLAB="/home/student/Matlab"
cd $UPENN_MATLAB
make
if [ ! $? -eq 0 ]
then
    echo "Failed to build matlab tools"
    exit 1
fi

# Run dcm
cd "$API_HOME/UPennalizers/Player/Lib"
lua -e "_G.python_api_root = \"$API_HOME\"" run_dcm.lua & 
DCM_PID=$!
DCM_STATUS=$?

echo "Waiting a bit for DCM (5 seconds)"
sleep 5

echo "Starting cognition"
cd "$API_HOME/UPennalizers/Player"
lua -e "_G.python_api_root = \"$API_HOME\"" run_cognition.lua & 
COG_PID=$!
COG_STATUS=$?

# Create matlab startup file
# (To set the search paths)
cd "$API_HOME"
echo "addpath $UPENN_MATLAB" > startup.m
echo "Starting the logger (matlab)"
matlab -nodesktop -r "Logger"

read -p "Waiting for keypress (quits)"

kill $COG_PID
kill $DCM_PID

exit 0 
