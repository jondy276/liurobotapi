#!/bin/sh

# Runs a python program with the api on darwin

# Just check number of arguments first
if [ $# -ne 1 ];
then
    echo "Run program on darwin"
    echo "usage: ./opRun.sh /path/to/code/folder/"
    echo "note: not to the file, it's the path to the folder, with or without trailing slash"
    echo "note 2: the entire folder will be sent to the robot, including subfolders"
    echo "note 3: the name of your main code file should be main.py"
    exit 1
fi

# Ping the robot to make sure it's online
ping -q -c 2 darwin
if [ $? -ne 0 ];
then
    echo "Darwin does not appear to be online, check the robot"
    exit 2
fi

# Rsync program folder to student home on robot 
rsync -vcaE --exclude=".*" --delete $(dirname "$1")/ student@darwin:/home/student/StudentProgram/

if [ $? -ne 0 ];
then
    echo "Failed to transfer the user code, shure you gave the right path?"
    exit 3
fi

# Start user program in python wrapper
# (the wrapper does things such as loading extra libraries and start dcm)
ssh student@darwin "sh ~/PythonApi/Scripts/opWrapper.sh /home/student/StudentProgram/"$(basename $1)