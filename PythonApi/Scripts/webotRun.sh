#!/bin/sh

# Run a program under webots
# (upennalizer needs to be built for webots use)
# (controller needs to be built)

# Usage:
# ./webotRun.sh /path/to/code/folder/

if [ $# -ne 1 ];
then
    echo "Runs a program inside the webots simulator"
    echo "Usage: ./webotRun.sh /path/to/code/folder/"
    exit 1
fi

# Create some usefull variables
API_HOME=$(dirname $(dirname $(readlink -f $0)))
USER_PROGRAM_FOLDER=$(readlink -f $1)

# Check that the api is built for webots with darwin
if [ ! -f "$API_HOME"/current_platform ] ||
    [ ! $(cat "$API_HOME"/current_platform) = "webots_darwin" ];
then
    echo "The api is not built for use with webots" 
    exit 2
fi
# Set the env variable that controls which platform the api works against
# (for one, this controls the servo configuration which differs between webots and darwin)
export API_PLATFORM=$(cat "$API_HOME/current_platform")

# First, write controller program path
# (we do this becouse webots does not pass thrue variables)
echo "$USER_PROGRAM_FOLDER" > "$API_HOME"/WebotsController/darwin_path_team_1

# Make all but one controller of team 1 to shutdown
echo "1" > "$API_HOME/WebotsController/player_id"

# Second, link our controllers to the robostadium controller folder
rm -rf "$WEBOTS_HOME"/projects/contests/robotstadium/controllers/darwin-op_team_0
rm -rf "$WEBOTS_HOME"/projects/contests/robotstadium/controllers/darwin-op_team_1
ln -s "$API_HOME"/WebotsController/ "$WEBOTS_HOME"/projects/contests/robotstadium/controllers/darwin-op_team_0
ln -s "$API_HOME"/WebotsController/ "$WEBOTS_HOME"/projects/contests/robotstadium/controllers/darwin-op_team_1

# Disable team 0 controller (removing path file means the controller will quit)
rm -rf "$API_HOME"/WebotsController/darwin_path_team_0

# Lastly, start webots with the robostadium world, running only controller 0 for one team
exec "$WEBOTS_HOME"/webots "$WEBOTS_HOME"/projects/contests/robotstadium/worlds/robotstadium_darwin-op_vs_darwin-op.wbt

# Sometimes webots won't kill the python process  
killall -s 9 python

exit 0
