#!/bin/sh

if [ $# -ne 0 ];
then 
    echo "Builds the api for use with webots"
    echo "usage: ./opBuild.sh"
    exit 1
fi

# Get path to api
API_HOME=$(dirname $(dirname $(readlink -f $0)))

# Build lunatic
sh "$API_HOME/Scripts/lunaticBuild.sh"

cd "$API_HOME"/UPennalizers/Lib/
if [ ! -f "$API_HOME"/current_platform ] ||
    [ ! $(cat "$API_HOME"/current_platform) = "webots_darwin" ];
then
    make clean
fi

make setup_webots_op

cd "$API_HOME"/UPennalizers/WebotsController
make clean
make

cd "$API_HOME"/Lib
ln -s "$API_HOME"/UPennalizers/WebotsController/controller.so controller.so

echo "webots_darwin" > "$API_HOME"/current_platform

exit 0