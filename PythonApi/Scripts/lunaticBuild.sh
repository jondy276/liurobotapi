#!/bin/sh

# This is an internal build script that builds the lunatic lib

API_HOME=$(dirname $(dirname $(readlink -f $0)))

mkdir -p "$API_HOME/Lib"

cd "$API_HOME/Robot/Lib/lunatic-python"
make

cp "$API_HOME/Robot/Lib/lunatic-python/build/lib.linux-i686-2.7/lua.so" "$API_HOME/Lib"

exit 0