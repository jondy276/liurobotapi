#!/bin/sh

# Run a program under webots on all the darwins 
# (two darwin teams facing each other)

# Usage:
# ./webotRunDarwins.sh /path/to/code/folder/team1/ /path/to/code/folder/team2/

if [ $# -ne 2 ];
then
    echo "Runs a program inside the webots simulator"
    echo "Usage: ./webotRunTeams.sh /path/to/code/folder/team0/ /path/to/code/folder/team1/"
    exit 1
fi

# Create some usefull variables
API_HOME=$(dirname $(dirname $(readlink -f $0)))
USER_TEAM_0=$(readlink -f $1)
USER_TEAM_1=$(readlink -f $2)

# Check that the api is built for webots with darwin
if [ ! -f "$API_HOME"/current_platform ] ||
    [ ! $(cat "$API_HOME"/current_platform) = "webots_darwin" ];
then
    echo "The api is not built for use with webots" 
    exit 2
fi

# Set the env variable that controls which platform the api works against
# (for one, this controls the servo configuration which differs between webots and darwin)
export API_PLATFORM=$(cat "$API_HOME/current_platform")

# First, write controller programs path
# (we do this becouse webots does not pass thrue variables)
echo "$USER_TEAM_0" > "$API_HOME"/WebotsController/darwin_path_team_0
echo "$USER_TEAM_1" > "$API_HOME"/WebotsController/darwin_path_team_1

# Remove player restrictions (all robots are playing)
rm -f "$API_HOME/WebotsController/player_id"

# Second, write our controller to the robostadium controller folder
rm -rf "$WEBOTS_HOME"/projects/contests/robotstadium/controllers/darwin-op_team_0
rm -rf "$WEBOTS_HOME"/projects/contests/robotstadium/controllers/darwin-op_team_1
ln -s "$API_HOME"/WebotsController/ "$WEBOTS_HOME"/projects/contests/robotstadium/controllers/darwin-op_team_0
ln -s "$API_HOME"/WebotsController/ "$WEBOTS_HOME"/projects/contests/robotstadium/controllers/darwin-op_team_1

# Lastly, start webots with the robostadium world
exec "$WEBOTS_HOME"/webots "$WEBOTS_HOME"/projects/contests/robotstadium/worlds/robotstadium_darwin-op_vs_darwin-op.wbt

# Sometimes webots won't kill the python process  
killall -s 9 python

exit 0
