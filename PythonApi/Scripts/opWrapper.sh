#!/bin/sh

# This script is suppose to run _on_the_robot_ !

# It starts the background dcm process and
# then starts the main user program file inside the python wrapper

# Usage: ./opWrapper.sh /path/to/user/top/folder

if [ $# -ne 1 ];
then
    echo "./opWrapper.sh /path/to/user/top/folder"
    exit 1
fi

# The robot is allways the same player in the same team
export PLAYER_NUMBER=1
export TEAM_NUMBER=1

API_HOME=$(dirname $(dirname $(readlink -f $0)))
USER_PROGRAM_FOLDER=$(readlink -f $1)

# Check that the api is built for webots with darwin
if [ ! -f "$API_HOME"/current_platform ] ||
    [ ! $(cat "$API_HOME/current_platform") = "darwin" ];
then
    echo "The api is not built for use on darwin" 
    exit 2
fi
# Set the env variable that controls which platform the api works against
# (for one, this controls the servo configuration which differs between webots and darwin)
export API_PLATFORM=$(cat "$API_HOME/current_platform")

# Move to UPennalizer lib folder, since dcm uses lots of relative paths
cd "$API_HOME"/UPennalizers/Player/Lib
lua run_dcm.lua & 
DCM_PID=$!
DCM_STATUS=$?

echo "Waiting a bit for DCM (5 seconds)"
sleep 5

# Move back home
cd ~

# Set up the path to the user program
export PYTHONPATH="$PYTHONPATH":"$HOME"/PythonApi 

# Run the user program (main.py)
python -u "$USER_PROGRAM_FOLDER"/main.py

# We should not leave dcm running between sessions
kill $DCM_PID

# Kill any remaing lua or python programs
killall -s 9 lua python

exit 0
