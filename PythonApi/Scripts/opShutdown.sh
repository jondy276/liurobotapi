#!/bin/sh

# Shutdowns the darwin

# You need to watch it's head for five green flashes.
# Then after those you can switch the power of.
# (failing to follow this procedure will prevent darwin to boot the next time)

ssh student@darwin "sudo halt"
