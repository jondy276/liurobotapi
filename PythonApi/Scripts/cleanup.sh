#!/bin/sh

# This script cleans any binaries out of the api

if [ $# -ne 0 ];
then 
    echo "usage: ./cleanup.sh"
    exit 1
fi

API_HOME=$(dirname $(dirname $(readlink -f $0)))

# Remove platform file
rm "$API_HOME"/current_platform

# Cleanup of /Lib
rm -rf "$API_HOME"/Lib/*

# Cleanup of lunatic
rm -rf "$API_HOME"/Src/lunatic-python/build

# Cleanup of Upennalizers
cd "$API_HOME"/UPennalizers/Lib
make clean
cd "$API_HOME"/UPennalizers/WebotsController
make clean

exit 0
