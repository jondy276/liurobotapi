""" The upenn setup module provides a simple interface to initiate
different parts of the upennalizer lua and c modules.

NOTE: dcm and vision systems cannot be started from here. 
(They are started by the run script provided)
"""

# We need access to the lua state
import lua


def setup_lua_path(python_api_root):
    """ Setup the path variables inside the lua state to point to the correct places """
    # Path to the upennalizers player folder
    upennPlayerPath = python_api_root + "/UPennalizers/Player"

    # Path to lua c modules
    upennLuaCpath = upennPlayerPath + "/Lib/?.so;"
    lua.globals().package.cpath = upennLuaCpath + lua.globals().package.cpath

    # The various lua modules used by upennalizers (relative to the player folder)
    upennLuaPaths = [ '/?.lua;', '/Util/?.lua;', '/Config/?.lua;', '/Lib/?.lua;', '/Dev/?.lua;', 
                      '/Motion/?.lua;', '/Motion/keyframes/?.lua;', '/Vision/?.lua;','/World/?.lua;' ]
    for playerRelativePath in upennLuaPaths:
        currentRequirePath = upennPlayerPath + playerRelativePath
        lua.globals().package.path = currentRequirePath + lua.globals().package.path


    # Save the path to the root directory in lua namespace
    lua.globals().python_api_root = python_api_root
        
