
"""
Provides a wrapper for printing lua tables
"""

import lua

lua.globals().package.path = lua.globals().python_api_root + "/Robot/Util/?.lua;" + lua.globals().package.path

lua.require("lua_printer")

def print_table(lua_table):
    """Prints a lua table"""
    print(lua.globals().lua_printer.table_print(lua_table))
