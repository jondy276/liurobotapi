
module(...,package.seeall)

function tabcmp( x, y )
   if #x ~= #y then
      return nil
   end
   for k,v in pairs(x) do
      if x[k] ~= y[k] then
	 return nil
      end
   end
   for k,v in pairs(y) do
      if x[k] ~= y[k] then
	 return nil
      end
   end
   return 1
end