
import lua

"""
This module helps when converting lua tables to and from different 
python types (list and dictionaries)
""" 

class UnsupportedConversion(Exception):
    def __init__(self, message):
        self.message = message

def toTable( listOrDict ):
    """ Converts eigther a python list, dictionary or tuple to a lua table """
    # Create lua table
    table = lua.eval("{}")
    # Maby there is a more typeindependent way of doing this?
    # Iterators?
    if isinstance(listOrDict, (list, tuple)):
        for i in range(len(listOrDict)):
            table[i+1] = listOrDict[i]
        return table
    elif isinstance(listOrDict, dict):
        for k,v in listOrDict.iteritems():
            table[k] = v
        return table
    else:
        raise UnsupportedConversion("only lists or dictionaries can be converted at this point")


def toDict( luaTable ):
    """ 
    Converts a lua table to a python dictionary
    Throws a KeyIsOfWrongType exception a key in the table is of a 
    incompatable type
    """
    # "Better" type check?
    if type( luaTable ) is not type( lua.eval("{}") ):
        raise UnsupportedConversion("toDict must take a lua table")
    d = {}
    key = lua.eval("nil")
    while True:
        # Lua multiple returns are converted to tuples
        ret = lua.globals().next( luaTable, key )
        if ret is None:
            break
        else:
            key = ret[ 0 ]
        # Type check on key
        d[ key ] = luaTable[ key ] 
    return d

def toList( luaTable ):
    """ 
    Converts a lua table to a python list
    The first key in the table has to be zero 
    """
    # Type check on table
    lst = []
    key = None
    while True:
        # Lua multiple returns are converted to tuples
        ret = lua.globals().next( luaTable, key )
        if ret is None:
            break
        else:
            key = ret[ 0 ]
            if type(key) is not int:
                raise UnsupportedConversion("table passed to toList kan only have integer keys")
        lst.append( ret[ 1 ] )
    return lst
