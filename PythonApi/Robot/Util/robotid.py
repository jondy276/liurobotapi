
"""
This module allows a python program to get the team and player number of the robot
"""

import os

class TeamNumberNotSet(Exception):
    """Raised if the team number environment variable is not set"""
    pass

class PlayerNumberNotSet(Exception):
    """Raised if the player number environment variable is not set"""
    pass

class ApiPlatformNotSet(Exception):
    """Raised if the platform invironment variable is not set"""
    pass

def get_platform():
    _platform = os.environ.get("API_PLATFORM")
    if not _platform:
        raise ApiPlatformNotSet()
    return _platform

def get_team_number():
    """Returns the number of the team this robot is on"""
    _team = os.environ.get("TEAM_NUMBER")
    if not _team:
        raise TeamNumberNotSet()
    return int(_team)

def get_player_number():
    """Returns the player number for this robot"""
    _player = os.environ.get("PLAYER_NUMBER")
    if not _player:
        raise PlayerNumberNotSet()
    return int(_player)
        
    
    

