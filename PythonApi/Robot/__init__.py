import sys, DLFCN
from os.path import dirname
# Stuff related to loading dynamic libraries
# because the lua state needs to load libraries to
sys.setdlopenflags(DLFCN.RTLD_NOW | DLFCN.RTLD_GLOBAL)

# And we need the path to the api root to setup the paths to upennalizers lua code 
_root = dirname(dirname(__file__))

# Add the lunatic API lib to the python path
sys.path.append(_root + "/Lib")


# Module to setup the lua paths to things inside upennalizers
from Util import upennsetup 

# Setup paths to upennalizer code
upennsetup.setup_lua_path(_root)

