"""
Functions for giving low level commands to the robot
More or less a interface to Lib/Platform/OP/Body/OPBody.lua 
"""

import time

# Get access to the lua vm
import lua

# Functions for converting to and from lua tables
from Robot.Util.TypeConverter.tableconverter import *

# Get control of the body using the lua body module
lua.require("Body")
_body = lua.globals().Body

def update():
    """ 
    Updates the robots control board so that the servos turn
    Must be called when using the get or set position functions
    """
    _body.update()

# Make dcm read all servos
_body.set_syncread_enable(1);
# Syncread takes time to enable
# (allso slows down reading, compared to only reading head)
time.sleep(0.2)

# We make this update call so that it reads servo positions and 
# hardness info before doing anything else
update()

"""
Here are the indexes for the lists returned from and passed to 
functions such as get_left_arm_position and set_left_arm_position.

Head:
0 - Yaw
1 - Pitch

Left/Right Arm:
0 - Shoulder pitch
1 - Shoulder roll
2 - Elbow yaw

Left/Right Leg:
(The legs cannot be controlled directly, for safety reasons)
0 - Hip Yaw
1 - Hip Roll
2 - Hip Pitch
3 - Knee Pitch
4 - Ankle Pitch
5 - Ankle Roll
"""

"""
The get position functions gives you a list of
numbers representing the angle of each servo in the body part, 
se the index section above.
"""

def get_head_position():
    """ Get head servo angles """
    return toList(_body.get_head_position())

def get_left_arm_position():
    """ Get a list of the angles of the left arm """
    return toList(_body.get_larm_position())

def get_right_arm_position():
    """ Get a list of the angles of the right arm """
    return toList(_body.get_rarm_position())

def get_left_leg_position():
    """ Get a list of the angles of the left leg """
    return toList(_body.get_lleg_position())

def get_right_leg_position():
    """ Get a list of the angles of the right leg """
    return toList(_body.get_rleg_position())

def get_position():
    """Returns a list with all servo position"""
    return toList(_body.get_sensor_position())

"""
The set position functions allow direct control
of the arm and head servos of the robot.
The legs are left out intentionally for safety.
The robotbody.update function must be called when using these functions

Each function have two variations, one that takes each angle
separatly and one that takes a list or dictionary of the angles.
Se the index section at the top of the file.
"""  

def set_head_position(yaw, pitch):
    """Set the angle of the head servos"""
    _body.set_head_command(toTable([yaw,pitch]),_body.indexHead)
def set_head_position_list(values):
    _body.set_head_command(toTable(values),_body.indexHead)

def set_left_arm_position(shoulder_pitch, shoulder_roll, elbow_yaw):
    """ Set the angle of the left arm servos """
    _body.set_larm_command(toTable([shoulder_pitch, shoulder_roll, elbow_yaw]), _body.indexLArm)
def set_left_arm_position_list(values):
    _body.set_larm_command(toTable(values), _body.indexLArm)

def set_right_arm_position(shoulder_pitch, shoulder_roll, elbow_yaw):
    """ Set the angle of the right arm servos """
    _body.set_rarm_command(toTable([shoulder_pitch, shoulder_roll, elbow_yaw]), _body.indexRArm)
def set_right_arm_position_list(values):
    _body.set_rarm_command(toTable(values), _body.indexRArm)

"""
The hardness functions control how hard the servos will try to 
hold their angle. You need to explicitly set the hardness when
using the set position functions.
The functions takes a value between 0 and 1, where 0 is a relaxed (but not powered down) servo
and 1 is full (very strong, not recomended) power.
""" 

def set_body_hardness(hardness):
    """ Controls the power of the servos in the body """
    # Read the entire body
    _positions = get_position()
    # Write new command
    _body.set_actuator_command(toTable(_positions))
    # Set hardness
    if isinstance(hardness, (list,dict)):
        hardness = toTable(hardness)
    _body.set_body_hardness(hardness)

def set_head_hardness(hardness):
    """ 
    Set the hardness of the servos controlling the head.
    Takes a value between 0 and 1 where 0 is relaxed.
    """
    # Set the command to the current angle
    set_head_position_list(get_head_position())
    # Set the hardness of the head servos
    if isinstance(hardness, (list,dict)):
        hardness = toTable(hardness)
    _body.set_head_hardness(hardness)

def set_left_arm_hardness(hardness):
    """ 
    Set the hardness of the servos controlling the left arm.
    Takes a value between 0 and 1 where 0 is relaxed.
    """
    # Set the command to the current angle
    set_left_arm_position_list(get_left_arm_position())
    # Set the hardness of the arm servos
    if isinstance(hardness, (list,dict)):
        hardness = toTable(hardness)
    _body.set_larm_hardness(hardness)

def set_right_arm_hardness(hardness):
    """ 
    Set the hardness of the servos controlling the right arm.
    Takes a value between 0 and 1 where 0 is relaxed.
    """
    # Set the command to the current angle
    set_right_arm_position_list(get_right_arm_position())
    # Set the hardness of the arm servos
    if isinstance(hardness, (list,dict)):
        hardness = toTable(hardness)
    _body.set_rarm_hardness(hardness)

def set_left_leg_hardness(hardness):
    """ 
    Set the hardness of the servos controlling the left leg.
    Takes a value between 0 and 1 where 0 is relaxed.
    """
    # Set the command to the current angle
    set_left_leg_position_list(get_left_leg_position())
    # Set the hardness of the arm servos
    if isinstance(hardness, (list,dict)):
        hardness = toTable(hardness)
    _body.set_lleg_hardness(hardness)

def set_right_leg_hardness(hardness):
    """ 
    Set the hardness of the servos controlling the right leg.
    Takes a value between 0 and 1 where 0 is relaxed.
    """
    # Set the command to the current angle
    set_right_leg_position_list(get_right_leg_position())
    # Set the hardness of the arm servos
    if isinstance(hardness, (list,dict)):
        hardness = toTable(hardness)
    _body.set_rleg_hardness(hardness)

"""
Functions for controlling the leds in the head
"""

def set_eyes_led(r, g, b):
    """
    Set the eyes leds to specified RGB-color, in values from 0 - 31
    Take approx 4ms before the change is made
    """ 
    _body.set_actuator_eyeled(toTable([r,g,b]))

def set_head_led(r,g,b):
    """
    Set the head led to specified RGB-color, in values from 0 - 31
    Take approx 8ms before the change is made
    """
    _body.set_actuator_headled(toTable([r,g,b]))


#def get_battery_level():
#    """Seems to not work correctly with Darwin-OP, just returns 10"""
#    return Body.get_battery_level()

"""
Functions for reading the state of the buttons on the back of the robot
"""

def get_back_buttons():
    """Return the state of the two buttons on the back of Darwin as an array"""
    return toList(_body.get_sensor_button())

def is_middle_button_pressed():
    """Returns true if Middle button is pressed, otherwise false"""
    return get_back_buttons()[0] == 1

def is_left_button_pressed():
    """Returns true if left button is pressed, otherwise false"""
    return get_back_buttons()[1] == 1
