""" 
Functions for reading the accelerometer, gyro and the combined values as a angle
"""

import lua
from Robot.Util.TypeConverter.tableconverter import *
import math

lua.require("Body")
_body = lua.globals().Body


def get_gyro_RPY():
    """Returns the same values as getImuGyro(), at least on Darwin"""
    return toList(_body.get_sensor_imuGyrRPY())

def get_gyro():
    """Return the gyro data from the IMU"""
    return toList(_body.get_sensor_imuGyr())

def get_acc():
    """Return the Accelerometer data from the IMU"""
    return toList(_body.get_sensor_imuAcc())


total_rotations = [0,0,0] # The total number of wraps, positive means wrap in positive direction, negative means wrap in negative direction
""" The number of whole rotations, positive is in poitive (left) direction, negative is in the other direction"""

_last_angles = toList(_body.get_sensor_imuAngle())

def get_angle():
    """
    Return the ImuAngle in Roll (tilt sideways), Pitch (tilt front/back), Yaw (rotate)
    The values are Continuous, which means they will never wrap around some value.
    """
    global _last_angles, total_rotations
    _angles = toList(_body.get_sensor_imuAngle())

    #print "New Angle " + str(_angles[2])
    #print "LastAngle " + str(_last_angles[2])
    #print "wraps " + str(total_rotations)

    # We consider the value has wrapped if the value has changed more than PI 
    for i in range(len(_angles)):
        if _angles[i] > _last_angles[i] + math.pi: # If the angle has increased by more than PI we detect a negative wrap
            total_rotations[i] -= 1
        
        if _angles[i] < _last_angles[i] - math.pi: # If the angle has decreased by more than PI we detect a positive wrap
            total_rotations[i] += 1


    _last_angles = _angles[:] # Make sure to copy the values, not the address

    # Add a number of total rotations
    for i in range(len(_angles)):
        _angles[i] += total_rotations[i]*2*math.pi


    return _angles
