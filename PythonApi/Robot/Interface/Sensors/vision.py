
# Statemachine that gives the location of the ball, goal and lines

import time

# Get access to the lua internals
import lua

from Robot.Util.TypeConverter.tableconverter import toDict

# Impring lua libs
lua.require("Config")
lua.require("World")
lua.require("Vision")
lua.require("HeadTransform")

# Lua libs
_world = lua.globals().World
_vision = lua.globals().Vision
_vcm = lua.globals().vcm
_headtransform = lua.globals().HeadTransform
_config = lua.globals().Config
_detect_goal = lua.globals().detectGoal

# Detection booleans and time of detection
_has_new_ball = False
_ball_t = 0
_has_new_goal = False
_goal_t = 0
_has_new_line = False
_line_t = 0

# Fps related globals
_img_n = 0
_img_t = 0
_fps = 0

def entry():
    _world.entry(_world)
    # Setup upenn vision state machine
    _vision.entry(_vision)

def update():
    """
    Update the vision processing state machine
    Returns true if there are new observations of the ball, the goal or any lines
    (This must be called to update the statemachine and perform processing)
    """
    _image_processed = _vision.update(_vision)
    _world.update_odometry()
    if _image_processed:
        # Update fps and image information
        global _img_n
        _img_n = _img_n + 1
        global _fps
        global _img_t
        _fps = (1 / (time.time() - _img_t))
        _img_t = time.time()
        # Update the world (for ball info)
        _world.update_vision(_world)

        # Update the detection booleans
        global _has_new_ball, _ball_t
        global _has_new_goal, _goal_t
        global _has_new_line, _line_t
        _has_new_ball = (_vcm.get_ball_detect() == 1)
        if _has_new_ball:
            _ball_t = time.time()
        _has_new_goal = (_vcm.get_goal_detect() == 1)
        if _has_new_goal:
            _goal_t = time.time()
        _has_new_line = (_vcm.get_line_detect() == 1)
        if _has_new_line:
            _line_t = time.time()
        return True
    else:
        return False

def exit():
    _vision.exit(_vision)
    _world.exit(_world)

"""
Ball, Goal and Line objects are returned by the get_ball, get_goal and get_line
functions and can then give the user things like position and angle to them.
"""

class Ball():
    """Describes an observation of the ball"""

    def __init__(self,x,y,t):
        """ 
        x and y are the 2d cordinates relative to the robot,
        t is the time the ball was detected
        """
        self.x = x
        self.y = y
        self.t = t

    def get_angle(self):
        """Gives the angle to the ball"""
        return get_head_angles(self.get_position())

    def get_position(self):
        """Gives the relative position of the ball"""
        return (self.x,self.y)

    def get_time(self):
        """Returns the time elapsed since the object was detected"""
        return time.time() - self.t

    def __str__(self):
        return "Ball at ("+str(self.x)+", "+str(self.y)+") time "+str(self.t)

class Goal():
    """Describes an observation of the goal"""
    
    """This is an observation of the left goal post"""
    LeftPost = 1
    """This is an observation of the right goal post"""
    RightPost = 2
    """This is an observation of the entire goal"""
    BothPosts = 3
    """Detected a single post, but could not determine which one"""
    SinglePost = 0

    def __init__(self, x, y, z, t, team, goal_type):
        """Takes the absolute cordinates, the time of detection and the team number"""
        self.x = x
        self.y = y
        self.z = z
        self.t = t
        self.team = team
        self.goal_type = goal_type

    def get_angle(self):
        """Gives the angle to the goal"""
        return get_head_angles((self.x,self.y,self.z))

    def get_position(self):
        """Gives the relative position of the goal"""
        return (self.x, self.y)

    def get_time(self):
        """Returns the time elapsed since the object was detected"""
        return time.time() - self.t
    
    def get_team(self):
        """Returns a number identifying the team that the goal belongs to"""
        return self.team
    def get_type(self):
        """
        Returns a tuple telling if the observation is only one post or the entire goal
        It allso tells which goal post post it is
        """
        return self.goal_type
        
    def __str__(self):
        _type_str = ""
        if self.goal_type == Goal.LeftPost:
            _type_str = "Left"
        elif self.goal_type == Goal.RightPost:
            _type_str = "Right"
        elif self.goal_type == Goal.BothPosts:
            _type_str = "Both"
        else:
            _type_str = "Single"
        return "Goal at ("+str(self.x)+", "+str(self.y)+") time "+str(self.t)+" "+_type_str

class Line():
    """A field line"""

    def __init__(self,cx, cy, ex, ey, t):
        """ 
        Create a new field line, cx/cy are the center cordinates and 
        ex/ey are the end cordinates 
        """
        self.cx = cx
        self.cy = cy
        self.ex = ex
        self.ey = ey
        self.t = t

    def get_position(self):
        """Get the center of the line"""
        return (self.cx,self.cy)

    def get_direction(self):
        """Get the direction of the line (in vector form)"""
        return (self.cx-self.ex,self.cy-self.ey)

    def get_angle(self):
        """Gives the angle to look at the "center" of the line"""
        return get_head_angles(self.get_position())

    def get_time(self):
        """Returns the time elapsed since the object was detected"""
        return time.time() - self.t

    def __str__(self):
        return "Line at ("+str(self.cx)+", "+str(self.cy)+") time "+str(self.t)

"""
These methods gives access to the latest observations of the ball, the goal and
any field lines.
"""

def has_new_ball_observation():
    global _has_new_ball
    return _has_new_ball

def has_new_goal_observation():
    global _has_new_goal
    return _has_new_goal

def has_new_line_observation():
    global _has_new_line
    return _has_new_line

def get_fps():
    """
    Returns the current number of frames per second read from the camera.
    (Only works if the update function is called frequently)
    Returns a tuple with image number and framerate: (nr, fps)
    """
    global _img_n
    global _fps
    return (_img_n, _fps)

def debug(status):
    if status:
        _detect_goal.debug = True
    else:
        _detect_goal.debug = False

def get_ball():
    """Get the latest observation of the ball"""
    # get from _world.ball
    global _has_new_ball, _ball_t
    if _has_new_ball:
        _has_new_ball = False
    return Ball(_world.ball.x,_world.ball.y,_ball_t)
    
def get_goal():
    """Get the latest observation of the goal"""
    global _has_new_goal, _goal_t
    if _has_new_goal:
        _has_new_goal = False
    tmp = _vcm.get_goal_v1()

    _team = 0
    if _vcm.get_goal_color() == _vision.colorYellow:
        _team = 1
    elif _vcm.get_goal_color() == _vision.colorCyan:
        _team = 0
        
    _goal_type = _vcm.get_goal_type()

    _goal = Goal(tmp[1],tmp[2],tmp[3],_goal_t,_team,_goal_type)

    return _goal

def get_line():
    """Get any line currently within view"""
    global _has_new_line
    _has_new_line = False
    return Line(_vcm.get_line_vcentroid()[1], _vcm.get_line_vcentroid()[2],
                _vcm.get_line_vendpoint()[1], _vcm.get_line_vendpoint()[2], 
                _line_t)

"""
These are supportfunctions used inside the vision system
""" 
    
def get_head_angles(v):
    """
    Get the angels to look at the cordinates in v
    which is in the format (x, y) or (x, y, z)
    Returns (yaw, pitch)
    """
    if len(v) == 2:
        ret = _headtransform.ikineCam(v[0], v[1], _config.vision.ball_diameter)
    elif len(v) == 3:
        ret = _headtransform.ikineCam(v[0], v[1], v[2])
    return (ret[0], ret[1])

