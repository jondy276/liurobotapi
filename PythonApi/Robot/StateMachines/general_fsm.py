
""" A simple state machine implementation used by the various python modules to
keep an internal state """

class StateMachine(object):

    class Transition(object):
        
        def __init__( self, fromState, eventName, toState ):
            self.fromState = fromState
            self.eventName = eventName
            self.toState = toState

        def valid_transition( self, currentState, currentEvent ):
            """ Check if this transistion is valid from a certain state given an event"""
            return self.fromState == currentState and self.eventName == currentEvent

    class UnknownEvent(Exception):
        """ Raised when an event did not match any known transition """
        def __init__( self, currentState, event ):
            self.currentState = currentState
            self.event = event
        def __str__( self ):
            return "State: " + repr( self.currentState ) + " Event: " + repr( self.event )

    class UnknownState(Exception):
        """ Is raised when the user tries to use a unknown state
        
        States has to be added before they can be added to transitions or set as
        the current state """
        def __init__( self, message ):
            self.message = message
        def __str__( self ):
            return repr( self.message )

    class NoInternalState(Exception):
        """ Raised when using functions on a StateMachine that has not ben entered
        
        (You are missing an entry call somewhere) """
        pass

    class AllreadyInState(Exception):
        """ Raised when trying to entry into a statemachine allready in a state
        
        (Needs to exit first) """
        pass

    def __init__( self, entryState ):
        """ The statemachine needs to have at least one state
        
        This state will be the default state to enter 
        when stm.entry() is called """
        # States stored with thier name as key
        self.states = [ entryState ]
        # Transitions stored with the from-state as key
        self.transitions = []
        self.currentState = None
        self.entryState = entryState

    def entry( self ):
        if self.currentState is None:
            self.currentState = self.entryState
        else:
            raise StateMachine.AllreadyInState()
        return self.currentState.entry()

    def update( self ):
        """ Call the current state update function, 
        
        Depending on the return value from the state function the state
        machine might enter a new state """
        if self.currentState is None:
            raise StateMachine.NoInternalState() 
        event = self.currentState.update()
        if event is None:
            return 
        else:
            self.switch_state_by_event( event )
                
    def exit( self ):
        if self.currentState is None:
            raise StateMachine.NoInternalState()
        else:
            ret = self.currentState.exit()
            self.currentState = None
            return ret
    
    def switch_state_by_event( self, event ):
        """ Changes the current state based on an event
        
        Raises exception if the event does not match any transition"""
        if self.currentState is None:
            raise StateMachine.NoInternalState()
        for tr in self.transitions:
            if tr.valid_transition( self.currentState, event ):
                self.currentState.exit()
                self.currentState = tr.toState
                tr.toState.entry()
                return True
            else:
                continue
        raise StateMachine.UnknownEvent( self.currentState, event )

    def set_state( self, state ):
        """ Set state bypasses the statemachine entirely!
        
        Might leave states in a bad internal state! """
        if state not in self.states:
            raise StateMachine.UnknownState("cannot set current state to a unknown state")
        self.currentState = state
        return True

    def get_state( self ):
        return self.currentState

    def add_state( self, newState ):
        """ Add another state to the statemachine
        
        Unless transistions to (and usually from) the state, the
        state will not be usefull in any way. """
        if newState not in self.states:
            self.states.append( newState )
        return True

    def add_transition( self, fromState, event, toState ):
        """ Add a transistion to the statemachine.
        
        A transition is when fromState returns an string that 
        matches a certain event, the state machine then enters toState 
        """
        if (fromState not in self.states) or (toState not in self.states):
            raise StateMachine.UnknownState("cannot add transitions to unknown states")
        self.transitions.append( StateMachine.Transition( fromState, event, toState ) )
        return True
