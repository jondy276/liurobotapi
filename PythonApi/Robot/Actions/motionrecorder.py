
""" The motion recorder module allows the program to record and replay movements """

import lua

def record():
    """ Calls lua code to start recording motions from 
    the motor angle sensors 
    Not implemented yet
    """
    pass

def replay():
    """ Takes control over the motors specified in a motion 
    and plays the corresponding motion 

    Not implemented yet
    """
    pass

def update(stop):
    """ Not implemented yet """
