import lua

import motion

lua.require("kick")
_kick = lua.globals().kick

active = _kick.active






def forward_left():
    """
    Stops and does a kick forward with the left leg
    When the kick is done the robot continues to walk with the former speed
    """
    if motion.activated_walk:
        # Change the state in motion.lua to kick. Does run the entry/exit methods
        # Does an normal transition back to walk when the kick is done.
        motion._event("kick")
        _kick.set_kick("kickForwardLeft")
    else:
        raise Exception("Call motion.start_walk() first!")


def forward_right():
    """
    Stops and does a kick forward with the right leg
    When the kick is done the robot continues to walk with the former speed
    """
    if motion.activated_walk:
        motion._event("kick")# Change state in the motion fsm (from the walk state)
        _kick.set_kick("kickForwardRight")
    else:
        raise Exception("Call motion.start_walk() first!")


def right():
    """
    Stops and does a kick to the right with the left leg
    When the kick is done the robot continues to walk with the former speed
    """
    if motion.activated_walk:
        motion._event("kick")# Change state in the motion fsm (from the walk state)
        _kick.set_kick("kickSideLeft")
    else:
        raise Exception("Call motion.start_walk() first!")


def left():
    """
    Stops and does a kick to the left with the right legx
    When the kick is done the robot continues to walk with the former speed
    """
    if motion.activated_walk:
        motion._event("kick")# Change state in the motion fsm (from the walk state)
        _kick.set_kick("kickSideRight")
    else:
        raise Exception("Call motion.start_walk() first!")


