"""
Interface to Player/Motion/Motion.lua
Is a module instead of a class to not be able to instansiate it multiple times, ie. as a Singleton
"""
import os

import lua
import walk
lua.require("Motion")
lua.require("Body")
_motion = lua.globals().Motion
_body = lua.globals().Body
_FSM = _motion.sm

activated_walk = False

def entry():
    _motion.entry()

def update():
    _motion.update()
    _body.update()

def exit():
    _motion.exit()



def _event(event):
    """
    Change the event immediately, if there is a proper transition.
    Does run the entry- and exit-methods.
    """
    _motion.event(event)# What if there is no proper transition? Does nothing?


def sit():
    """Sit down (from stance) and entering relax state"""
    _FSM.set_state(_FSM, "sit")

def stand_still():
    """Stands still in upright position. The robot stands up if he is sitting"""
    #_FSM.set_state(_FSM, "stance")
    if os.environ.get('API_PLATFORM') == "webots_darwin":
        _FSM.set_state(_FSM, "standstill")
    elif os.environ.get('API_PLATFORM') == "darwin":
        _FSM.set_state(_FSM, "stance")



def get_up():
    """
    Get up from front or back depending on the angle of the imu
    Continue trying until he is in sitting position
    """
    _FSM.set_state(_FSM, "standup")

def relax():
    """
    Relax all the joints for 1s. Good to call when we are about to fall
    Actually the lua motion state machine relaxes the joints for 0.3 seconds 
    in the case of falling
    """
    _FSM.set_state(_FSM, "relax")

def start_walk():
    """ Initiate the walking algorithm
    Call to the walk.entry / walk.exit methods, from Motion.lua
    Should we also call walk.start()?? 
    """
    global activated_walk
    # The name passed must be the same as the module file name.. Jon
    _FSM.set_state(_FSM, "NewWalk")
    walk._start()
    activated_walk = True


def stop_walk():
    global activated_walk
    walk._stop()
    activated_walk = False



## Use stance instead, because this one has 0 bodytilt (for webots)
#def stand_still():
#    """Stand still in upright position. The robots stands up if he is sitting"""
#    _FSM.set_state(_FSM, "standstill")


## Use relax instead, the state falling is used internally in Motion's State Machine
#def falling():
#    """Relax all the joints (more or less)"""
#    _FSM.set_state(_FSM, "falling")
    
#def kick():
#    """Maybe better to call event("kick")"""
#    _FSM.set_state(_FSM, "kick")



