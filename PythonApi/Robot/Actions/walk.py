"""
Interface to Player/Motion/NewWalk.lua
Is a module instead of a class to not be able to instansiate it multiple times, ie. as a Singleton
"""

import lua
import motion
from Robot.Util.TypeConverter.tableconverter import *


lua.require("walk")
_walk = lua.globals().walk


# Called from the motion.lua when in walk-state
#def update():
#    """
#    Returns Stop, when stopping sequence is done
#    Nothing, otherwise 
#    """
#    return _walk.update()


def get_velocity():
    """ Returns the current velocity array. 
    1st element: The velocity in forward/backward direction 
    2nd element: The velocity in left/right direction
    3rd element: The rotation velocity in positive direction
    """
    # Units are unknown, maybe 0.3m/s and some logarithmic angle velocity
    return toList(_walk.get_velocity())


def set_velocity(vx,vy,va):
    """
    Sets the walking velocity/direction

    Arguments:
    vx -- the velocity in forward/backward direction
    vy -- the velocity in left/right direction
    va -- the angle-velocity around Darwin axle
    The units are approx 0.3m/s and some logaritmic angle velocity...
    Normal values are 0-0.05 for walk and 0-0.4 for rotation
    """
    if motion.activated_walk:
        _walk.set_velocity(vx,vy,va)
    else:
        raise Exception("Call motion.start_walk() first!")

def _start():
    """
    Switch state to walk and initiate variables
    Depricated: Should use motion.start_walk() instead
    """
    _walk.start()

def _stop():
    """
    Switch state to stop and initiate variables
    Depricated: Should use motion.stop_walk() instead
    """
    _walk.stop()



#def is_walking():
#    """Returns true if the robot is walking"""
#    return _walk.active

def walk_forward(speed):
    """Walk forward with speed [0-1]"""
    set_velocity(speed, 0, 0);

def walk_backward(speed):
    """Walk backward with speed [0-1]"""
    set_velocity(-speed, 0, 0);

def walk_lateral_right(speed):
    """Walk sideways right with speed [0-1]"""
    set_velocity(0, -speed, 0);

def walk_lateral_left(speed):
    """Walk sideways left with speed [0-1]"""
    set_velocity(0, speed, 0);

def turn_right(speed):
    """Turn right with speed [0-1]"""
    set_velocity(0, 0, -speed);

def turn_left(speed):
    """Turn left with speed [0-1]"""
    set_velocity(0, 0, speed);



def walk_kick_left():
    """ Kick with left leg while walking """
    if motion.activated_walk:
        _walk.doWalkKickLeft()
    else:
        raise Exception("Call motion.start_walk() first!")


def walk_kick_right():
    """ Kick with right leg while walking """
    if motion.activated_walk:
        _walk.doWalkKickRight()
    else:
        raise Exception("Call motion.start_walk() first!")


def get_odometry():
    """ 
    Returns an array with 3 elements indicating the distance in X, Y and the rotation. 
    This method use dead recocknig, so the distance is not the actual distance. 
    1st element: Distance forward/backward, in m
    2nd element: Distance left/right, in m
    3rd element: Rotation in positive direction, in radians
    """
    # lua returns a tuple of relative pose and an absolute pose. Currently I can not see any difference between the two, so we return the absolute pose. I think they don't set u0 (relative this pose) to any number except {0,0,0}
    return toList(_walk.get_odometry()[1])


# Dont know the exact meaning of the values, and probably noone will use this
#def get_body_offset():
#    """ Return the distance between the torso and the feets """
#    return toList(_walk.get_body_offset())
