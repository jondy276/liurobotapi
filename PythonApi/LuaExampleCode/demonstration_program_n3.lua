
--[[
   Liu robot team 
   --]]

--UPennalizer initiation code

-- Get Platform for package path
cwd = '../Player';
local platform = os.getenv('PLATFORM') or '';
if (string.find(platform,'webots')) then cwd = cwd .. '/Player';
end
--print(platform)
--print(cwd)

-- Get Computer for Lib suffix
local computer = os.getenv('COMPUTER') or '';
if (string.find(computer, 'Darwin')) then
  -- MacOS X uses .dylib:
  package.cpath = cwd .. '/Lib/?.dylib;' .. package.cpath;
else
  package.cpath = cwd .. '/Lib/?.so;' .. package.cpath;
end



-- Add the various libraries to the path
package.path = cwd .. '/?.lua;' .. package.path;
package.path = cwd .. '/Util/?.lua;' .. package.path;
package.path = cwd .. '/Config/?.lua;' .. package.path;
package.path = cwd .. '/Lib/?.lua;' .. package.path;
package.path = cwd .. '/Dev/?.lua;' .. package.path;
package.path = cwd .. '/Motion/?.lua;' .. package.path;
package.path = cwd .. '/Motion/keyframes/?.lua;' .. package.path;
package.path = cwd .. '/Vision/?.lua;' .. package.path;
package.path = cwd .. '/World/?.lua;' .. package.path;

require("Config")
smindex = 0;

-- Various support libraries
require('vector')
require('getch')
require('util')

--Setup shared memmory
require('shm')
require('vcm') 
require('gcm')
require('wcm')
require('mcm')

--Setup body interface
require('Body')

--Camera and ball recognition / positioning 
require('Vision') -- Camera hight level camera interface
require('World') -- Positioning and world perception
require('HeadTransform') -- 2d to 3d mathematics

-- Give us a voice
require("Speak")

--Add path to finite state machine's controlling the robot body
package.path = cwd..'/BodyFSM/'..Config.fsm.body[smindex+1]..'/?.lua;'..package.path;
package.path = cwd..'/HeadFSM/'..Config.fsm.head[smindex+1]..'/?.lua;'..package.path;

-- Load finite state machines
BodyFSM=require('BodyFSM');
HeadFSM=require('HeadFSM');

-- Low level motion control
require('Motion');

-- Non-blocking terminal io 
getch.enableblock(1);

-- Initiate libraries and state machines
World.entry();
Vision.entry();

HeadFSM.entry();
Motion.entry();

-- Turn on head servos
Body.set_head_hardness({0.4,0.4});
-- Head state machine is idle  
HeadFSM.sm:set_state("headIdle");
BodyFSM.sm:set_state("bodyIdle");
Speak.talk("Ready")
vel = vector.zeros(3);
vel[1]=0.1
vel[2]=0.1
vel[3]=0.1

while 1 do
   Body.set_syncread_enable(0); --read from only head servos

   -- Read commands from terminal
   local byte = string.byte(getch.get(),1);
   if byte == string.byte("q") then
      --Quit
      break
   elseif byte == string.byte("s") then
      --Start scanning
      Speak.talk("Looking for the red ball")
      HeadFSM.sm:set_state("headStart");
      BodyFSM.sm:set_state("bodyStart");
   elseif byte == string.byte("x") then
      Speak.talk("Going idle")
      HeadFSM.sm:set_state("headIdle");
      BodyFSM.sm:set_state("bodyIdle");
   end

   -- Now, let all the libraries update thier states
   if Vision.update() then
      --New frame available
      World.update_vision(); --Update the world
   end
   -- Run the statemachines controlling motion

   Body.update();
   Motion.update();
   HeadFSM.update();
   BodyFSM.update();
   --walk.set_velocity(unpack(vel));
   unix.usleep(0.005*1E6);
end 
Speak.talk("Terminating program")
HeadFSM.sm:set_state("headIdle");
BodyFSM.sm:set_state("bodyIdle");
-- Turn of servos
Body.set_head_hardness({0.0,0.0});
--Unload shared memmory, etc
Motion.exit();
Vision.exit();
World.exit();