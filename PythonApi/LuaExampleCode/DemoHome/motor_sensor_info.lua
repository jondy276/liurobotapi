
-- What to read (servo positions)
-- Body.set_syncread_enable(0);
--from run_dcm.lua
--0 Read Head only
--1 Read ALL
--2 Read Head and Leg
--3 Read Kankles only


-- Servos
-- list from OPBody.lua
indexHead = 1;			--Head: 1 2
nJointHead = 2;
indexLArm = 3;			--LArm: 3 4 5 
nJointLArm = 3; 		
indexLLeg = 6;			--LLeg: 6 7 8 9 10 11
nJointLLeg = 6;
indexRLeg = 12; 		--RLeg: 12 13 14 15 16 17
nJointRLeg = 6;
indexRArm = 18; 		--RArm: 18 19 20
nJointRArm = 3; 

