
--[[
   Demonstration wrapper program 
   Reads commands from stdin and runs other programs based on commands

   (commands will be written from voice recognition program)
--]]

require("io")

while 1 do
   local str = io.read()
   if str == "quit" then
      break;
   else
      print(str)
   end
end 