function setup_packages()
   local cwd = "../Player"
   -- Extra code needed here for weebots
   package.cpath = cwd .. '/Lib/?.so;' .. package.cpath;
   -- Extra code for libs built with mac
   -- Add the various libraries to the path

   package.path = cwd .. '/?.lua;' .. package.path;
   package.path = cwd .. '/Util/?.lua;' .. package.path;
   package.path = cwd .. '/Config/?.lua;' .. package.path;
   package.path = cwd .. '/Lib/?.lua;' .. package.path;
   package.path = cwd .. '/Dev/?.lua;' .. package.path;
   package.path = cwd .. '/Motion/?.lua;' .. package.path;
   package.path = cwd .. '/Motion/keyframes/?.lua;' .. package.path;
   package.path = cwd .. '/Vision/?.lua;' .. package.path;
   package.path = cwd .. '/World/?.lua;' .. package.path;
   
   package.path = './MotionRec/?.lua;' .. package.path;
   package.path = './?.lua;' .. package.path;

   require("Config") -- configure libs 
   smindex = 0; -- Which state machines to use (robot-specific)
   require("Body")
   require("Speak")
   require("getch")
   -- Now start all the stuff needed
end 

-- Use the above function to load the UPPennalizers modules
setup_packages()

getch.enableblock(1); -- set non-blocking terminal io 

Body.set_syncread_enable(1); -- read all servos

Body.set_actuator_command(Body.get_sensor_position()); --set servos to current positions
Body.set_head_hardness(0.1)
Body.set_larm_hardness(0.1)
Body.set_rarm_hardness(0.1)
Body.set_lleg_hardness(0.1)
Body.set_rleg_hardness(0.1)

while 1 do
   local byte = string.byte(getch.get(),1);
   if byte == string.byte("q") then
      break;
   end
end

Body.set_head_hardness(0.0)
Body.set_larm_hardness(0.0)
Body.set_rarm_hardness(0.0)
Body.set_lleg_hardness(0.0)
Body.set_rleg_hardness(0.0)
Body.set_syncread_enable(1); -- read all servos