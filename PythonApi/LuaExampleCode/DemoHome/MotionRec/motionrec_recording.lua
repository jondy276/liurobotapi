
--[[
   --]]

module(..., package.seeall);

local frames = nil

local resolution = 0.02;
local sTime = 0.0
local cTime = 0.0
local delta = 0.1



function get_motion()
   if frames then
      local frame = {servos={},keyframes=frames}
      for k=1,20 do
	 frame.servos[k] = k
      end
      return frame;
   else
      return "no_motion"
   end
end


function reset_servos()
   
   Body.set_rleg_command(Body.get_rleg_position()); 
   Body.set_lleg_command(Body.get_lleg_position());
   unix.usleep(200000); --wait for controller to write servos
   Body.set_rleg_hardness(1.0)
   Body.set_lleg_hardness(1.0)

   Body.set_rarm_command(Body.get_rarm_position()); 
   Body.set_head_command(Body.get_head_position()); 
   Body.set_larm_command(Body.get_larm_position()); 
   unix.usleep(200000); --wait for controller to write servos
   --Body.set_rarm_hardness(0.0)
   --Body.set_larm_hardness(0.0)
   --Body.set_head_hardness(0.0)

end

function entry(event)
   --print("entry - recording")
   --print(event)
   --[[
      First lock legs in current position
      unlock arms and head
      --]]
   Body.set_syncread_enable(1); -- read all
   unix.usleep(200000); --wait for controller to read servos
   Body.set_torque_enable(0)
   
   frames = {servos={}}
   for i=1,20 do
      frames.servos[i]=i
   end
end


function update()
   if (Body.get_time()-sTime) > delta then
      --Time for a new frame
      local j = Body.get_sensor_position();
      frames[#frames+1]={angles={},duration=delta,stiffness={}}
      for k,v in pairs(j) do
	 frames[#frames].angles[k] = v;
	 frames[#frames].stiffness[k] = 0.5;
      end
      sTime = Body.get_time()
   end
end

function exit(event)
   --print("exit - recording")
   --print(event)
   Body.set_torque_enable(1)
   frames = nil
end