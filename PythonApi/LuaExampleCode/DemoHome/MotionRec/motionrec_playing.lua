
--[[
   Playing a motion (keyframes)
--]]

module(..., package.seeall); 

require("fsm")
require("keyframe")

--print("MotionRec - playing fsm setup done")

function entry(event)
   -- check for a set motion
   -- Tell everyone you are playing a motion
   -- Enter into keyframe
   --print("entry - playing")
   --print(event)

   Body.set_actuator_command(Body.get_sensor_position()); --set servos to current positions
   Body.set_head_hardness(1.0)
   Body.set_larm_hardness(1.0)
   Body.set_rarm_hardness(1.0)
   Body.set_lleg_hardness(0.5)
   Body.set_rleg_hardness(0.5)

end

function update()
   if(keyframe.get_queue_len()==0) then
      return "done"
   end
   keyframe.update()
   --unix.usleep(10000);
end

function exit(event)
   -- Exit keyframe
   -- keyframe.exit()
   --print("exit - playing")
   --print(event)
end

