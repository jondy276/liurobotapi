
--[[
This module handles errors during 
playback and recording
--]]

module(..., package.seeall)

require("fsm")
require("Speak")

function entry(event)
   Speak.talk("Warning, there was an error in Motion Recorder")
   Speak.talk("For safety i will relax all motors and go idle")
   print("entry - error")
   print(event)
end

function update()
   return "done"
end

function exit(event)
   print("exit - error")
   print(event)
end
