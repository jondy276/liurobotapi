

--[[
   Library for recording movements of robots
   (Currently for DarwinOp, 
   and only for upper body)
--]]

module(..., package.seeall)
   
require("motionrec_idle")
require("motionrec_recording")
require("motionrec_playing")
require("motionrec_error")

require("keyframe")

sm = fsm.new(motionrec_idle);
sm:add_state(motionrec_recording)
sm:add_state(motionrec_playing)
sm:add_state(motionrec_error)

sm:set_transition(motionrec_recording, 'done',motionrec_idle)
sm:set_transition(motionrec_recording, 'error',motionrec_error)
sm:set_transition(motionrec_recording, 'abort',motionrec_idle)

sm:set_transition(motionrec_playing, 'done',motionrec_idle)
sm:set_transition(motionrec_playing, 'error',motionrec_error)
sm:set_transition(motionrec_playing, 'abort',motionrec_idle)

sm:set_transition(motionrec_error, 'done',motionrec_idle)

function play(table_or_name)
   if sm:get_current_state() == motionrec_idle then
      if type(table_or_name)=="table" then
	 keyframe.motData["current"]=table_or_name
	 keyframe.entry()
	 keyframe.do_motion("current")
      elseif type(table_or_name) == "string" then
	 -- load file
	 -- Start to play
	 if not keyframe.motData[table_or_name] then
	    keyframe.load_motion_file(table_or_name)
	 end
	 keyframe.entry()
	 keyframe.do_motion(table_or_name)
      elseif (not table_or_name) and keyframe.motData["current"] then
	 keyframe.entry()
	 keyframe.do_motion("current")
      else
	 return "there is no motion to play" 
      end
      sm:set_state("motionrec_playing") 
      return "playing"
   else
      return "busy"
   end
end

-- Save a keyframe (latest recording)
function save(name)
   if sm:get_current_state() == motionrec_idle then
      return save_keyframe(name)
   else
      return "busy"
   end
end 

function load(name)
   if sm:get_current_state() == motionrec_idle then
      keyframe.load_motion_file(name)
      keyframe.motData["current"]=keyframe.motData[name]
   else
      return "busy"
   end
end

function save_keyframe(name)
   local f = assert(io.open(name,'w'))
   if not f then
      return "could not open file"
   elseif not keyframe.motData["current"] then
      return "no recorded motion"
   else
      local fmotion = keyframe.motData["current"] 
      local frames = fmotion.keyframes
      f:write('local mot={};\n');
      
      f:write('mot.servos={');
      for i=1,#(fmotion.servos) do
	 f:write(i,',');
      end
      f:write('};\n');
      
      f:write('mot.keyframes={');
      for i=1,#frames do
	 f:write("  {\n    angles={\n");
	 for j=1,#(fmotion.servos) do
	    f:write(frames[i].angles[j],",");
	 end
	 f:write("\n    },\n");
	 f:write("\n    stiffness={\n");
	 if frames[i].stiffness then
	    for j=1,#(fmotion.servos) do
	       f:write(frames[i].stiffness[j],",");
	    end
	 end
	 f:write("\n    },\n");
	 f:write("duration = "..frames[i].duration.."; \n  },\n");
      end
      f:write("};\n\nreturn mot;")
      f:close();
      return "saved"
   end
end


--Start recording
function record() 
   if not (sm:get_current_state() == motionrec_idle) then
      return "busy"
   else
      sm:set_state("motionrec_recording") 
      return "recording"
   end
end


-- Stop whatever you are doing
function abort()
   if not (sm:get_current_state() == motionrec_idle) then
      if sm:get_current_state() == motionrec_recording then
	 keyframe.motData["current"]=motionrec_recording.get_motion()
      elseif sm:get_current_state() == motionrec_playing then
	 -- relax motors
      end
      sm:exit("abort") -- exit state
   end
end

function entry()
   sm:entry()
   sm:set_state("motionrec_idle")
end

function update()
   return sm:update()
end

function exit()
   sm:exit()

   Body.set_syncread_enable(1); -- read all servos
   unix.usleep(200000); --wait for controller to read servos
   Body.set_actuator_command(Body.get_sensor_position()); --set servos to current positions
   Body.set_head_hardness(0.0)
   Body.set_larm_hardness(0.0)
   Body.set_rarm_hardness(0.0)
   Body.set_lleg_hardness(0.0)
   Body.set_rleg_hardness(0.0)
   Body.set_syncread_enable(0); --disable reading
end