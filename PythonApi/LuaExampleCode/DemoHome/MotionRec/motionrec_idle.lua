
--[[
   Going idle (state)
--]]

module(..., package.seeall);

function entry(entry)

   -- Tell everyone you are realxed
   --print("entry - idle")
   --print(entry)
   -- Relax all motors 

   Body.set_actuator_command(Body.get_sensor_position()); --set servos to current positions
   Body.set_head_hardness(0.0)
   Body.set_larm_hardness(0.0)
   Body.set_rarm_hardness(0.0)
   Body.set_lleg_hardness(0.0)
   Body.set_rleg_hardness(0.0)
end

function update()
   -- Do nothing
end

function exit(event)
   -- No need to do anything
   --print("exit - idle")
   --print(entry)
end