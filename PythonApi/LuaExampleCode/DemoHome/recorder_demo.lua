
--[[
   Demonstration program for the recording library
--]]

-- Setup, initiate etc all the necessary UPPennalizers libs
function setup_packages()
   local cwd = "../Player"
   -- Extra code needed here for weebots
   package.cpath = cwd .. '/Lib/?.so;' .. package.cpath;
   -- Extra code for libs built with mac
   -- Add the various libraries to the path

   package.path = cwd .. '/?.lua;' .. package.path;
   package.path = cwd .. '/Util/?.lua;' .. package.path;
   package.path = cwd .. '/Config/?.lua;' .. package.path;
   package.path = cwd .. '/Lib/?.lua;' .. package.path;
   package.path = cwd .. '/Dev/?.lua;' .. package.path;
   package.path = cwd .. '/Motion/?.lua;' .. package.path;
   package.path = cwd .. '/Motion/keyframes/?.lua;' .. package.path;
   package.path = cwd .. '/Vision/?.lua;' .. package.path;
   package.path = cwd .. '/World/?.lua;' .. package.path;
   
   package.path = './MotionRec/?.lua;' .. package.path;
   package.path = './?.lua;' .. package.path;

   require("Config") -- configure libs 
   smindex = 0; -- Which state machines to use (robot-specific)
   require("Body")
   require("Speak")
   require("getch")
   require("io")
   MotionRec = require("MotionRec");
   -- Now start all the stuff needed
end 

-- Use the above function to load the UPPennalizers modules
setup_packages()

getch.enableblock(1); -- set non-blocking terminal io 

Body.set_syncread_enable(1); -- read all servos

-- Start the player/recorder fsm
MotionRec.entry()

function read_name()
   local name = ""
   while 1 do
      local str = getch.get()
      if string.len(str)>0 then
	 name = name .. str
      end
      if string.find(name,"\n") then
	 break
      end
      end
   name = string.sub(name,1,string.len(name)-1)
   return name
end

local latest_motion = nil

print("Keys:")
print("r","records")
print("s","stop recording (empty line prevents saving)")
print("l","opens and loads a file")
print("p","play latest recording")
print("q","quits program (both during recording and playing)")

-- Update and interaction loop
while 1 do
   local byte = string.byte(getch.get(),1);
   if byte == string.byte("q") then
      MotionRec.abort()
      break;
   elseif byte == string.byte("r") then
      -- start recording
      Speak.talk("Recording motion")
      print(MotionRec.record())
   elseif byte == string.byte("l") then
      -- load file
      Speak.talk("Input name of motion to load")
      print(MotionRec.load(read_name()..".lua"))
   elseif byte == string.byte("p") then
      -- load file
      Speak.talk("Playing")
      print(MotionRec.play())
   elseif byte == string.byte("s") then
      MotionRec.abort()
      print("Name of new motion (empty line skips saving)");
      local name = read_name()
      if string.len(name)>0 then
	 print(MotionRec.save(name..".lua"))
      else
	 print("did not save")
      end
   end 
   MotionRec.update() -- Update the module, controls the robot during recording and playing
end
MotionRec.exit() --klose down player/recorder 




