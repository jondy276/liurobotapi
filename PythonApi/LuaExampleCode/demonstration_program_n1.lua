
--[[
   Liu robot team 
   --]]

--UPennalizer initiation code

-- Get Platform for package path
cwd = '.';
local platform = os.getenv('PLATFORM') or '';
if (string.find(platform,'webots')) then cwd = cwd .. '/Player';
end
--print(platform)
--print(cwd)

-- Get Computer for Lib suffix
local computer = os.getenv('COMPUTER') or '';
if (string.find(computer, 'Darwin')) then
  -- MacOS X uses .dylib:
  package.cpath = cwd .. '/Lib/?.dylib;' .. package.cpath;
else
  package.cpath = cwd .. '/Lib/?.so;' .. package.cpath;
end

-- Add the various libraries to the path
package.path = cwd .. '/?.lua;' .. package.path;
package.path = cwd .. '/Util/?.lua;' .. package.path;
package.path = cwd .. '/Config/?.lua;' .. package.path;
package.path = cwd .. '/Lib/?.lua;' .. package.path;
package.path = cwd .. '/Dev/?.lua;' .. package.path;
package.path = cwd .. '/Motion/?.lua;' .. package.path;
package.path = cwd .. '/Motion/keyframes/?.lua;' .. package.path;
package.path = cwd .. '/Vision/?.lua;' .. package.path;
package.path = cwd .. '/World/?.lua;' .. package.path;

-- Sets up the different settings for fsm's, teams, etc 
-- require('Config')
-- High level motion control (state machine)
-- require('Motion')
-- Terminal input (non and blocking)
require('getch')
-- Access to low level body function
-- (such as get_head_positon)
require('Body')
require('Speak')

getch.enableblock(1); -- not sure if it is blocking or not

--[[
   Program code
--]]

while 1 do
   local byte = string.byte(getch.get(),1);
   if byte == string.byte("q") then
      break
   elseif byte == string.byte("h") then 
      local v = Body.get_head_position()
      print("Vector: ");
      print(v[2])
   elseif byte == string.byte("g") then
      Body.set_actuator_eyeled( {0,31,0} );
   elseif byte == string.byte("b") then
      Body.set_actuator_eyeled( {0,0,31} );
   elseif byte == string.byte("t") then
      Speak.talk("Hello, nao, wake up")
   end
   Body.update()
end 
return 0;
