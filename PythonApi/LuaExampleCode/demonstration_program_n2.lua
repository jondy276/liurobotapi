
--[[
   Liu robot team 
   --]]

--UPennalizer initiation code

-- Get Platform for package path
cwd = '.';
local platform = os.getenv('PLATFORM') or '';
if (string.find(platform,'webots')) then cwd = cwd .. '/Player';
end
--print(platform)
--print(cwd)

-- Get Computer for Lib suffix
local computer = os.getenv('COMPUTER') or '';
if (string.find(computer, 'Darwin')) then
  -- MacOS X uses .dylib:
  package.cpath = cwd .. '/Lib/?.dylib;' .. package.cpath;
else
  package.cpath = cwd .. '/Lib/?.so;' .. package.cpath;
end

-- Add the various libraries to the path
package.path = cwd .. '/?.lua;' .. package.path;
package.path = cwd .. '/Util/?.lua;' .. package.path;
package.path = cwd .. '/Config/?.lua;' .. package.path;
package.path = cwd .. '/Lib/?.lua;' .. package.path;
package.path = cwd .. '/Dev/?.lua;' .. package.path;
package.path = cwd .. '/Motion/?.lua;' .. package.path;
package.path = cwd .. '/Motion/keyframes/?.lua;' .. package.path;
package.path = cwd .. '/Vision/?.lua;' .. package.path;
package.path = cwd .. '/World/?.lua;' .. package.path;

-- Sets up the different settings for fsm's, teams, etc 
require('Config')
-- Terminal input (non and blocking)
require('getch')
require('Body')
require('unix')
require('vector')
--require('HeadFSM')

getch.enableblock(1); 

--[[
   Program code
--]]



local p = nil
local spd = 0.05
local headangle=Body.get_head_position()
Body.set_head_hardness({0.4,0.4});
while 1 do

   local byte = string.byte(getch.get(),1);
   if byte == string.byte("q") then
      break
   elseif byte == string.byte("w") then
      headangle[2] = headangle[2] + spd
      p = true
   elseif byte == string.byte("s") then
      headangle[2] = headangle[2] - spd
      p = true
   elseif byte == string.byte("a") then
      headangle[1] = headangle[1] + spd
      p = true
   elseif byte == string.byte("d") then
      headangle[1] = headangle[1] - spd
      p = true
   end
   if p then
      p = nil
      print(headangle[1])
      print(headangle[2])
      print("")
   end

   Body.set_head_command(headangle);
   unix.usleep(0.005*1E6);
   Body.update()
end 
Body.set_head_hardness({0.0,0.0});