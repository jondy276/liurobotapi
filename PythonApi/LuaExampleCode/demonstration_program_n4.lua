
--[[
Walking
--]]


-- Get Platform for package path
cwd = '.';
local platform = os.getenv('PLATFORM') or '';
if (string.find(platform,'webots')) then cwd = cwd .. '/Player';
end
--print(platform)
--print(cwd)

-- Get Computer for Lib suffix
local computer = os.getenv('COMPUTER') or '';
if (string.find(computer, 'Darwin')) then
  -- MacOS X uses .dylib:
  package.cpath = cwd .. '/Lib/?.dylib;' .. package.cpath;
else
  package.cpath = cwd .. '/Lib/?.so;' .. package.cpath;
end

-- Add the various libraries to the path
package.path = cwd .. '/?.lua;' .. package.path;
package.path = cwd .. '/Util/?.lua;' .. package.path;
package.path = cwd .. '/Config/?.lua;' .. package.path;
package.path = cwd .. '/Lib/?.lua;' .. package.path;
package.path = cwd .. '/Dev/?.lua;' .. package.path;
package.path = cwd .. '/Motion/?.lua;' .. package.path;
package.path = cwd .. '/Motion/keyframes/?.lua;' .. package.path;
package.path = cwd .. '/Vision/?.lua;' .. package.path;
package.path = cwd .. '/World/?.lua;' .. package.path;

require("Config")
smindex = 0;

-- Various support libraries
require('vector')
require('getch')
require('util')

--Setup shared memmory
require('shm')
require('vcm') 
require('gcm')
require('wcm')
require('mcm')

--Setup body interface
require('Body')

--Camera and ball recognition / positioning 
require('Vision') -- Camera hight level camera interface
require('World') -- Positioning and world perception
require('HeadTransform') -- 2d to 3d mathematics

-- Give us a voice
require("Speak")

--Add path to finite state machine's controlling the robot body
package.path = cwd..'/BodyFSM/'..Config.fsm.body[smindex+1]..'/?.lua;'..package.path;
package.path = cwd..'/HeadFSM/'..Config.fsm.head[smindex+1]..'/?.lua;'..package.path;

-- Load finite state machines
BodyFSM=require('BodyFSM');
HeadFSM=require('HeadFSM');

-- Low level motion control
require('Motion');

-- Non-blocking terminal io 
getch.enableblock(1);

-- Initiate libraries and state machines
World.entry();
Vision.entry();
HeadFSM.entry();
Motion.entry();

HeadFSM.sm:set_state("headIdle");

Speak.talk("Setup done")

while 1 do
   local byte = string.byte(getch.get(),1);
   if byte == string.byte("q") then
      break;
   end
   if Vision.update() then
      --New frame available
      World.update_vision(); --Update the world
   end

   Body.update();
   Motion.update();
   HeadFSM.update();

   unix.usleep(0.005*1E6);   
end 
Speek.talk("ending program")