
"""
This testprogram prints team information for the running robot
and then exits.
"""

from Robot.Util import robotid

print("My Team: "+robotid.get_team_number()+" My number: "+robotid.get_player_number())

