
module(... or '', package.seeall)

-- Get Platform for package path
cwd = '.';
local platform = os.getenv('PLATFORM') or '';
if (string.find(platform,'webots')) then cwd = cwd .. '/Player';
end

-- Get Computer for Lib suffix
local computer = os.getenv('COMPUTER') or '';
if (string.find(computer, 'Darwin')) then
  -- MacOS X uses .dylib:
  package.cpath = cwd .. '/Lib/?.dylib;' .. package.cpath;
else
  package.cpath = cwd .. '/Lib/?.so;' .. package.cpath;
end

package.path = cwd .. '/?.lua;' .. package.path;
package.path = cwd .. '/Util/?.lua;' .. package.path;
package.path = cwd .. '/Config/?.lua;' .. package.path;
package.path = cwd .. '/Lib/?.lua;' .. package.path;
package.path = cwd .. '/Dev/?.lua;' .. package.path;
package.path = cwd .. '/Motion/?.lua;' .. package.path;
package.path = cwd .. '/Motion/keyframes/?.lua;' .. package.path;
package.path = cwd .. '/Vision/?.lua;' .. package.path;
package.path = cwd .. '/World/?.lua;' .. package.path;
package.path = cwd .. '/HeadFSM/OpPlayer/?.lua;' .. package.path;

require('Config')
require('unix')
require('vcm')
require('gcm')
require('wcm')
require('Body')
require('HeadFSM')
require('World')
require('Vision')

Vision.entry()
HeadFSM.entry()
Body.set_head_hardness(0.5)
HeadFSM.sm:set_state('headStart')

_img_t = 0
_img_n = 0

while true do
   if Vision.update() then
      World.update_vision()
      _img_n = _img_n + 1
      print(_img_n .. "FPS: " .. (1 / (unix.time()-_img_t)) )
      _img_t = unix.time()
   end
   HeadFSM.update()
end

Vision.exit()
HeadFSM.exit()